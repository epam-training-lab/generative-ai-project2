# Movie Database API

## Description

The Movie Database API is a RESTful service developed using Spring Boot and Spring Data MongoDB. It provides a platform to perform CRUD operations on movies, actors, and directors. The API allows users to add, retrieve, update, and delete movie information, including title, release date, runtime, and associated actors and directors. Additionally, it supports searching for movies by title or release date.

## Features

- CRUD operations on movies, including:
    - Adding a new movie
    - Retrieving all movies
    - Retrieving a specific movie by ID
    - Updating a movie
    - Deleting a movie
- Search functionality:
    - Search movies by title
    - Search movies by release date

## Technologies

- Spring Boot
- Spring Data MongoDB
- Java
- MongoDB

## Getting Started

### Prerequisites

- Java JDK 21 or later
- Gradle
- MongoDB installed and running on your local machine or accessible via a network connection

  Build the project:

For Maven:

bash

mvn clean install

For Gradle:

bash

gradle build

    Run the application:

For Maven:

bash

mvn spring-boot:run

For Gradle:

bash

gradle bootRun

The application will start and be accessible at http://localhost:8080.
Using the API

Refer to the provided Postman collection or API documentation for detailed endpoint usage. Here are a few examples to get started:

    Add a Movie:

    POST /api/movies

    json

    {
      "title": "Inception",
      "releaseDate": "2010-07-16",
      "runtime": 148,
      "actors": [
        {
          "name": "Leonardo DiCaprio",
          "role": "Cobb"
        }
      ],
      "directors": [
        {
          "name": "Christopher Nolan"
        }
      ]
    }

    Get All Movies:

    GET /api/movies

    Search Movies by Title:

    GET /api/movies/search?title=Inception

## Contributing

We welcome contributions! Please open an issue or submit a pull request for any improvements or bug fixes.
