package com.epam.movie.service;

import com.epam.movie.entity.Movie;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface MovieService {

    Movie saveMovie(Movie movie);
    List<Movie> getAllMovies();
    Optional<Movie> getMovieById(String id);
    Movie updateMovie(String id, Movie movie);
    void deleteMovie(String id);
    List<Movie> findMoviesByTitle(String title);
    List<Movie> findMoviesByReleaseDate(LocalDate releaseDate);

}
