package com.epam.movie.controller;

import com.epam.movie.entity.Movie;
import com.epam.movie.service.MovieService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(MovieController.class)
class MovieControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovieService movieService;

    @Autowired
    private ObjectMapper objectMapper;

    private Movie movie;

    @BeforeEach
    void setUp() {
        movie = new Movie("The Matrix", LocalDate.of(1999, 3, 31), 136, Collections.emptyList(), Collections.emptyList());
    }

    @Test
    void addMovieTest() throws Exception {
        given(movieService.saveMovie(any(Movie.class))).willReturn(movie);

        mockMvc.perform(post("/api/movies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(movie)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title").value(movie.getTitle()));
    }

    @Test
    void getMovieByIdTest() throws Exception {
        given(movieService.getMovieById(anyString())).willReturn(Optional.of(movie));

        mockMvc.perform(get("/api/movies/{id}", "1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value(movie.getTitle()));
    }

    @Test
    void getAllMoviesTest() throws Exception {
        given(movieService.getAllMovies()).willReturn(Collections.singletonList(movie));

        mockMvc.perform(get("/api/movies")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title").value(movie.getTitle()));
    }

    @Test
    void updateMovieTest() throws Exception {
        given(movieService.updateMovie(anyString(), any(Movie.class))).willReturn(movie);

        mockMvc.perform(put("/api/movies/{id}", "1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(movie)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value(movie.getTitle()));
    }

    @Test
    void deleteMovieTest() throws Exception {
        doNothing().when(movieService).deleteMovie(anyString());

        mockMvc.perform(delete("/api/movies/{id}", "1"))
                .andExpect(status().isNoContent());
    }

}