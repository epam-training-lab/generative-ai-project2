package com.epam.movie.service;

import com.epam.movie.entity.Movie;
import com.epam.movie.repository.MovieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
class MovieServiceImplTest {

    @Mock
    private MovieRepository movieRepository;

    @InjectMocks
    private MovieServiceImpl movieService;

    private Movie movie;

    @BeforeEach
    void setUp() {
        // Initialize your test data
        movie = new Movie("The Matrix", LocalDate.of(1999, 3, 31), 136, Collections.emptyList(), Collections.emptyList());

        when(movieRepository.findById(anyString())).thenReturn(Optional.of(movie));
        when(movieRepository.save(any(Movie.class))).thenReturn(movie);
        when(movieRepository.findAll()).thenReturn(List.of(movie));
        when(movieRepository.findByTitleContaining(anyString())).thenReturn(List.of(movie));
        when(movieRepository.findByReleaseDate(any(LocalDate.class))).thenReturn(List.of(movie));
    }

    @Test
    void saveMovieTest() {
        Movie savedMovie = movieService.saveMovie(movie);
        assertNotNull(savedMovie);
        assertEquals(movie.getTitle(), savedMovie.getTitle());
    }

    @Test
    void getAllMoviesTest() {
        List<Movie> movies = movieService.getAllMovies();
        assertFalse(movies.isEmpty());
        assertEquals(1, movies.size());
    }

    @Test
    void getMovieByIdTest() {
        Optional<Movie> foundMovie = movieService.getMovieById("someId");
        assertTrue(foundMovie.isPresent());
        assertEquals(movie.getTitle(), foundMovie.get().getTitle());
    }

    @Test
    void updateMovieTest() {
        // Assuming there's a method to update some details of the movie
        Movie updatedMovie = new Movie("The Matrix Reloaded", LocalDate.of(2003, 5, 15), 138, Collections.emptyList(), Collections.emptyList());
        when(movieRepository.findById(anyString())).thenReturn(Optional.of(movie));

        Movie result = movieService.updateMovie("someId", updatedMovie);
        assertNotNull(result);
        assertEquals(updatedMovie.getTitle(), result.getTitle());
    }

    @Test
    void deleteMovieTest() {
        // This test verifies the delete operation. Since the method doesn't return anything,
        // we're mainly ensuring that no exceptions are thrown and the repository method is called.
        assertDoesNotThrow(() -> movieService.deleteMovie("someId"));
        verify(movieRepository, times(1)).deleteById(anyString());
    }

}